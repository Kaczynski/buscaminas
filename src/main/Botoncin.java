package main;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;

public class Botoncin extends JButton {

    private int xx;
    private int yy;
    private boolean bombaaaa = false;
    private boolean selectedAsBombaaa = false;
    private int around = 0;
    private boolean descubierto = false;

    public Botoncin(int x, int y) {
        super();
        this.xx = x;
        this.yy = y;
        this.setText("");
        this.setBackground(Color.gray);
        this.setFocusPainted(false);
        this.setBorder(null);
        this.setForeground(Color.red);
        this.setPreferredSize(new Dimension(30, 30));
    }

    public int getXx() {
        return xx;
    }

    public void setXx(int xx) {
        this.xx = xx;
    }

    public boolean getBombaaaa() {
        return bombaaaa;
    }

    public void setBombaaaa(boolean bombaaaa) {
        this.bombaaaa = bombaaaa;
    }

    public int getAround() {
        return around;
    }

    public void setAround(int around) {
        this.around = around;
        if(around==1){
            setForeground(Color.blue);
        }else if(around==2){
            setForeground(Color.green);
        }else if(around==3){
            setForeground(Color.yellow);
        }else if(around>3){
            setForeground(Color.red);
        }
    }

    public int getYy() {
        return yy;
    }

    public void setYy(int yy) {
        this.yy = yy;
    }

    public boolean isSelectedAsBombaaa() {
        return selectedAsBombaaa;
    }

    public void setSelectedAsBombaaa(boolean selectedAsBombaaa) {
        this.selectedAsBombaaa = selectedAsBombaaa;
        if (selectedAsBombaaa) {
            this.setBackground(Color.ORANGE);
        } else {
            this.setBackground(Color.GRAY);
        }

    }

    public void descubrir() {
        descubierto = true;
        this.setBackground(Color.lightGray);
        if(bombaaaa){
            this.setBackground(Color.red);
            this.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/mine.png")));
        }
        this.setText((around==0)?"":Integer.toString( around));
	this.setBackground(Color.LIGHT_GRAY);
    }

    public boolean descubierto() {
        return descubierto;
    }

    void setDescubierto(boolean b) {
        this.descubierto = false;
    }

    void setear() {
        this.setBombaaaa(false);
        this.setAround(0);
        this.setText("");
        this.setIcon(null);
        this.setSelectedAsBombaaa(false);
        this.setDescubierto(false);
    }

}
