package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.Random;

import javax.swing.JPanel;

public class PanelJugable extends JPanel {

	Botoncin[][] campo;
        int minas = 0;
	public PanelJugable(int x, int y, int minas) {
		campo = new Botoncin[x][y];
                this.minas = minas;
		this.setLayout(new GridLayout(x, y, 1, 1));
		this.setPreferredSize(new Dimension(x * 30, y * 30));
	
		sembrarCampo();
		sembrarMinas(minas);
	}
	
	

	private void sembrarCampo() {
		for (int i = 0; i < campo.length; i++) {
			for (int x = 0; x < campo[i].length; x++) {
				campo[i][x] = new Botoncin(i, x);	
				this.add(campo[i][x]);
			}
		}
	}

        public void cosecharCampo() {
            for (Botoncin[] campo1 : campo) {
                for (Botoncin item : campo1) {
                    item.setear();
                }
            }
            sembrarMinas(minas);
	}

	public void sembrarMinas(int minas) {
		Random r = new Random();
		for (int i = 0; i < minas; i++) {
			int x = r.nextInt(campo.length), y = r.nextInt(campo[0].length);
			while (campo[x][y].getBombaaaa()) {
				x = r.nextInt(campo.length);
				y = r.nextInt(campo[0].length);
			}
			campo[x][y].setBombaaaa(true);
		}
		this.calcularCircundantes();
	}

	private void calcularCircundantes() {
		for (int i = 0; i < campo.length; i++) {
			for (int x = 0; x < campo[i].length; x++) {
				if (!campo[i][x].getBombaaaa()) {
					try {
						if (campo[i - 1][x].getBombaaaa()) {
							campo[i][x].setAround(campo[i][x].getAround() + 1);
						}
					} catch (ArrayIndexOutOfBoundsException w) {
					}
					try {
						if (campo[i - 1][x - 1].getBombaaaa()) {
							campo[i][x].setAround(campo[i][x].getAround() + 1);
						}
					} catch (ArrayIndexOutOfBoundsException w) {
					}
					try {
						if (campo[i - 1][x + 1].getBombaaaa()) {
							campo[i][x].setAround(campo[i][x].getAround() + 1);
						}
					} catch (ArrayIndexOutOfBoundsException w) {
					}
					try {
						if (campo[i + 1][x].getBombaaaa()) {
							campo[i][x].setAround(campo[i][x].getAround() + 1);
						}
					} catch (ArrayIndexOutOfBoundsException w) {
					}
					try {
						if (campo[i + 1][x + 1].getBombaaaa()) {
							campo[i][x].setAround(campo[i][x].getAround() + 1);
						}
					} catch (ArrayIndexOutOfBoundsException w) {
					}
					try {
						if (campo[i + 1][x - 1].getBombaaaa()) {
							campo[i][x].setAround(campo[i][x].getAround() + 1);
						}
					} catch (ArrayIndexOutOfBoundsException w) {
					}
					try {
						if (campo[i][x + 1].getBombaaaa()) {
							campo[i][x].setAround(campo[i][x].getAround() + 1);
						}
					} catch (ArrayIndexOutOfBoundsException w) {
					}
					try {
						if (campo[i][x - 1].getBombaaaa()) {
							campo[i][x].setAround(campo[i][x].getAround() + 1);
						}
					} catch (ArrayIndexOutOfBoundsException w) {
					}
					
				}
				
			}
		}
	}
	
	
	
	void mostrarLibres(Botoncin b) {
		if(b.getAround()==0) {
			b.descubrir();
			if((b.getXx() - 1)  > -1) {
				//System.out.println((b.getXx() - 1)+","+ (b.getYy() ));
				if(!this.campo[b.getXx() - 1 ][b.getYy()].descubierto()) {
					mostrarLibres(this.campo[b.getXx() - 1 ][b.getYy()]);
				}
			}
			
			if((b.getXx() + 1)  < this.campo.length) {
				//System.out.println((b.getXx() - 1)+","+ (b.getYy() ));
				if(!this.campo[b.getXx() + 1 ][b.getYy()].descubierto()) {
					mostrarLibres(this.campo[b.getXx() + 1 ][b.getYy()]);
				}
			}
			
			if((b.getYy() - 1)  > -1) {
				//System.out.println((b.getXx() - 1)+","+ (b.getYy() ));
				if(!this.campo[b.getXx()][b.getYy()-1].descubierto()) {
					mostrarLibres(this.campo[b.getXx() ][b.getYy() -1]);
				}
			}
			
			if((b.getYy() + 1)  < this.campo[0].length) {
				//System.out.println((b.getXx() - 1)+","+ (b.getYy() ));
				if(!this.campo[b.getXx() ][b.getYy() +1].descubierto()) {
					mostrarLibres(this.campo[b.getXx() ][b.getYy()+1]);
				}
			}
			
			
		}else {
			b.descubrir();
		}
	}
	
	
	void mostrarTodo() {
            for (Botoncin[] campo1 : campo) {
                for (Botoncin item : campo1) {
                    item.descubrir();
                }
            }
	}
	
}
