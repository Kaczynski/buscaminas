/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author lukas
 */
public class PlayersControler {
    ArrayList<Players> playersList;

    public PlayersControler(ArrayList<Players> playersList) {
        this.playersList = playersList;
    }

    public PlayersControler() {
        this.playersList = new ArrayList<>();
    }
    
    
    
    public ArrayList<Players> getPlayersList() {
        return playersList;
    }

    public void setPlayersList(ArrayList<Players> playersList) {
        this.playersList = playersList;
    }
    
    public boolean addPlayer(int score, String name){
        
        if (playersList.stream().anyMatch((players) -> (name.equals(players.getName())))) {
            return false;
        }
        Players p = new Players(score, name);
        playersList.add(p);
        Collections.sort(playersList);
        int t =0;
        ArrayList<Players> temp = new ArrayList<>();
        for (Players players : playersList) {
                if(t<10){
                    temp.add(players);
                }
                t++;
        }
        playersList=temp;
        return true;
    }
}
