package main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class View extends JFrame implements MouseListener, ActionListener {

    private final PanelJugable p;
    private final JPanel body;
    private JPanel panel;
    int score = 0;
    private JLabel label;
    private boolean gameover = false;
    private int user;
    private final int x = 10;
    private final int y = 10;
    private final int m = 10;
    Results r ;
    public View() {
        p = new PanelJugable(x, y, m);
        body = new JPanel(new BorderLayout());
        r = new Results(this, false);
        inits();
        this.setLocationRelativeTo(null);
    }

    private void inits() {
        this.setTitle("Buscaminas");
        r.setModalityType(java.awt.Dialog.ModalityType.APPLICATION_MODAL);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.r.jButton1.addActionListener(this);
        
        init();

        body.add(p, BorderLayout.CENTER);
        this.setContentPane(body);
        for (JButton[] b : this.p.campo) {
            for (JButton bt : b) {
                bt.addMouseListener(this);
            }
        }
        this.pack();
    }

    void geerateUser() {
        user = new Random().nextInt();
    }

    private void init() {
        panel = new JPanel(new BorderLayout());
        label = new JLabel("Label");
        label.setHorizontalAlignment(JLabel.CENTER);
        panel.setSize(new Dimension(100, 32));;
        panel.add(label, BorderLayout.CENTER);

        body.add(panel, BorderLayout.NORTH);

    }

    @Override
    public void mouseClicked(MouseEvent me) {
        if (!gameover) {
            if (me.getButton() == MouseEvent.BUTTON1) {
                if (me.getSource() instanceof Botoncin) {
                    this.mostrar((Botoncin) me.getSource());
                    if (((Botoncin) me.getSource()).descubierto()) {
                        score++;
                    }
                    score++;
                    label.setText("Score: " + Integer.toString(score));
                }
            }
            if (me.getButton() == MouseEvent.BUTTON3) {
                ((Botoncin) me.getSource()).setSelectedAsBombaaa(true);
            }
        }

    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseExited(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    void mostrar(Botoncin b) {
        if (b.getBombaaaa()) {
            this.p.mostrarTodo();
            gameover = true;
            JOptionPane.showMessageDialog(null, "Booom!!!!", "Game Over", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/main/485587.png")));
            r.setVisible(true);
            
            gameover = false;
        } else {
            this.p.mostrarLibres(b);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.r.jButton1){
            r.setScore(score);
        }
    }

}
